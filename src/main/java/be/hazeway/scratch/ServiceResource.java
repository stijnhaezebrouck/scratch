package be.hazeway.scratch;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceResource {

    @GetMapping("/services/myservice")
    public String myService() {
        return "myService";
    }

    @GetMapping("/services/anotherservice")
    public String anotherService() {
        return "anotherService";
    }
}
