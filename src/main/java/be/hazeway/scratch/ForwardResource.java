package be.hazeway.scratch;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.websocket.server.PathParam;

@RestController
public class ForwardResource {

    @GetMapping("/{restofthepath}")
    public ModelAndView forwardRoot(@PathVariable("restofthepath") String restofthepath) {
        return new ModelAndView("forward:/services/"+restofthepath);
    }
}
